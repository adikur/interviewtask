const assert = require('chai').assert;
const formatMoney = require('../src/helper/formatMoney')

describe('Format Money Test', function(){
    
    it('Sould return STRING ',function(){
        assert.typeOf(formatMoney(14000,2,'.',','),'string')
    });
    it('Sould return 14,000.00 ',function(){
        assert.equal(formatMoney(14000,2,'.',','),'14,000.00')
    });
    it('Sould return 14.000,00 ',function(){
        assert.equal(formatMoney(14000,2,',','.'),'14.000,00')
    });
    it('Sould return 14.000,0000 ',function(){
        assert.equal(formatMoney(14000,4,'.',','),'14,000.0000')
    });
    it('Send Null Sould return 0.00 ',function(){
        assert.equal(formatMoney(null,2,'.',','),'0.00')
    });
})