const dataReducer = require('../src/model/Data');
const assert = require('chai').assert;

describe ('Reducer Data Test',()=>{
    const state = {
        isLoading : false,
        currency: "USD",
        name: "United States Dollars",
        value: 10.0000,
        data:[
            {
                currency: "IDR",
                name: "Indonesia Rupiah",
                value: 14410.45,
            },
            {
                currency: "EUR",
                name: "Euro", 
                value: 0.8569,
            },
        ]
    };
    describe('getData()',()=>{
        it('Sould return a data list array ',()=>{
            const action = {
                type : 'GET_DATA'
            }
            const expected = {

                isLoading : false,
                currency: "USD",
                name: "United States Dollars",
                value: 10.0000,
                data:[
                    {
                        currency: "IDR",
                        name: "Indonesia Rupiah",
                        value: 14410.45,
                    },
                    {
                        currency: "EUR",
                        name: "Euro", 
                        value: 0.8569,
                    },
                ]

            };
            assert.deepEqual(dataReducer(state,action),expected)
        });
    })   




    describe('addData()',()=>{
        it('Sould return a new data in list array ',()=>{
            const action = {
                type : 'ADD_DATA',
                payload: {
                    currency: "SGD",
                    name: "Singapore Dolllar", 
                    value: 1.366758,
                }
            }
            const expected = {

                isLoading : false,
                currency: "USD",
                name: "United States Dollars",
                value: 10.0000,
                data:[
                    {
                        currency: "IDR",
                        name: "Indonesia Rupiah",
                        value: 14410.45,
                    },
                    {
                        currency: "EUR",
                        name: "Euro", 
                        value: 0.8569,
                    },
                    {
                        currency: "SGD",
                        name: "Singapore Dolllar", 
                        value: 1.366758,
                    },
                ]
            }

            assert.deepEqual(dataReducer(state,action),expected)
        });
    })   



    describe('removeData()',()=>{
        it('Sould return a data list array without removed item',()=>{
            const action = {
                type : 'REMOVE_DATA',
                payload: 1
            }
            const expected = {

                isLoading : false,
                currency: "USD",
                name: "United States Dollars",
                value: 10.0000,
                data:[
                    {
                        currency: "IDR",
                        name: "Indonesia Rupiah",
                        value: 14410.45,
                    },
                    
                ]
            }

            assert.deepEqual(dataReducer(state,action),expected)
        });
    })   



})