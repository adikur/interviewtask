const initialState = {
	
    base: "USD",
    date: "2018-08-03",
    rates: {
        AUD: 1.3539005868,
        BGN: 1.6877804625,
        BRL: 3.7447359337,
        CAD: 1.3017777011,
        CHF: 0.9948222299,
        CNY: 6.8342250604,
        CZK: 22.1410079392,
        DKK: 6.4313082499,
        GBP: 0.76846738,
        HKD: 7.8490680014,
        HRK: 6.3917846048,
        HUF: 276.7949603038,
        IDR: 14463.2982395582,
        ILS: 3.6933034173,
        INR: 68.6158094581,
        ISK: 106.6620642043,
        JPY: 111.580945806,
        KRW: 1124.5426303072,
        MXN: 18.6658612358,
        MYR: 4.0700724888,
        NOK: 8.2335174318,
        NZD: 1.4817052123,
        PHP: 53.0747324819,
        PLN: 3.67725233,
        RON: 3.9870555747,
        RUB: 63.2937521574,
        SEK: 8.9051605109,
        SGD: 1.3667587159,
        THB: 33.2645840525,
        TRY: 5.0878494995,
        USD: 1.0,
        ZAR: 13.3646013117,
        EUR: 0.8629616845
    }
    
}

export default (state = initialState, action) => {
    switch(action.type){
        case "GET_DATA_CURRENCY":{
            return{
                state
            }
        }
        default:{
            return state
        }
    }
}
