const initialState = {
    isLoading : false,
    currency: "USD",
    name: "United States Dollars",
    value: 10.0000,
    data:[
        {
            currency: "IDR",
            name: "Indonesia Rupiah",
            value: 14410.45,
        },
        {
            currency: "EUR",
            name: "Euro", 
            value: 0.8569,
        },
    ]

}


module.exports = (state = initialState, action) => {
    switch(action.type){
        case "GET_DATA":{
            return{
                ...state
            }
        }
        case "ADD_DATA":{
            return{
                ...state, data: [...state.data, action.payload]
            }
        }
        case "REMOVE_DATA":{
            //console.log("reducer "+action.payload)
            return{
                ...state , 
                data: [
                    ...state.data.slice(0, action.payload),
                    ...state.data.slice(action.payload+ 1)
                ]

            }
        }
        default:{
            return state
        }
    }
}
