//// action ////
export function getData(){
    return{
        type: "GET_DATA",
    }
}
export function addData(newData){
    return{
        type: "ADD_DATA",
        payload: newData
    }
}
export function removeData(index){
    return{
        type: "REMOVE_DATA",
        payload: index
    }
}
