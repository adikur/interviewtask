import { combineReducers } from 'redux';
import dataReducers from '../model/Data';
import dataCurrency from '../model/Currency';



const rootReducers = combineReducers({
    dataReducers,
    dataCurrency
})

export default rootReducers
