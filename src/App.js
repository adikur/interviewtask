import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import List from './components/List';
import Add from './components/Add';
import axios from 'axios';
import { connect } from 'react-redux';
import { compose } from 'recompose';


import { getData } from './model/DataAction';

const formatMoney = require('./helper/formatMoney');

const styles = theme => ({
 
  root: {
    flexGrow: 1,
    width: 350, 
    padding: 10,
    paddingTop: 5,
    paddingBottom:20,
    marginTop:10,
    marginBottom: 10,
    
  },
  header:{
    padding:10
  }
})

class App extends React.Component{

  componentWillMount(){


      axios.get('https://exchangeratesapi.io/api/latest?base=USD',{
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
      })
      
        .then(function (response) {
          console.log(response);

        })
        .catch(function (error) {
          console.log(error);

        });
        
        
  }

  getData = (event) => {
    this.props.getData();
   }

  render(){
    const { classes } = this.props;
    const data = this.props.dataReducers;
  
    return (
      <Grid 
        container
        direction="column"
        alignItems="center" >


            
        <Paper className={classes.root}>


            <Grid container item xs={12} className={classes.header} >

           


              <Grid item xs={12}  >
                    <Typography gutterBottom variant="subheading">
                      {data.currency} - {data.name}
                    </Typography>
              </Grid>
              
              <Grid item xs={12} sm container>

                <Grid item xs container direction="column" spacing={16}>
                  <Grid item xs>
                  
                    <Typography gutterBottom variant="title">{data.currency}</Typography>

                  </Grid>
                  
                </Grid>

                <Grid item>
                  <Typography variant="title">{formatMoney(data.value,2,'.',',')}</Typography>
                </Grid>

              </Grid>

            </Grid>


            <Grid
                  container
                  direction="column"      
                  alignItems="center"
                  >

              {
                data.data.map((data,index) => (
                  <List data={data} key={data.currency} index={index} baseCurrency={this.props.dataReducers.currency} baseValue={this.props.dataReducers.value} />
                ))
              }
              
              <Add/>

              
            </Grid>
        </Paper>
        
      </Grid>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};


 const mapStateToProps = state => ({
  ...state
 })
 const mapDispatchToProps = dispatch => ({
  getData: () => dispatch(getData())
 })

export default compose (
  withStyles(styles),
  connect(mapStateToProps,mapDispatchToProps)
)(App);

