import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import { removeData } from '../model/DataAction';
import { connect } from 'react-redux';
import { compose } from 'recompose';

const formatMoney = require('../helper/formatMoney');

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: 300,
    padding: theme.spacing.unit * 2,
    marginTop: 5,
    marginBottom:5,
    borderWidth:1,
    borderColor: '#d6d7da'
  },
  containerRemove: {
    width: 35,
    height: 90,
  },
  remove: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },
});

class List extends React.Component{ 


   removeIndex = event => {
    this.props.removeData(this.props.index);
    
  };

 
  render(){
    const { classes } = this.props;
    const data = this.props.data;
    const baseCurrency = this.props.baseCurrency;
    const baseValue = this.props.baseValue;
    const total = baseValue * this.props.data.value;
      

    return (
      <Paper className={classes.root}>
        <Grid container spacing={16}>
  
         
          
  
          <Grid item xs={12} sm container>
  
            <Grid item xs container direction="column" spacing={8}>
              <Grid item xs>
                <Typography gutterBottom variant="subheading">
                  {data.currency}
                </Typography>
                <Typography gutterBottom>{data.currency} -  {data.name}</Typography>
                <Typography color="textSecondary">1 {baseCurrency} = {data.currency} {data.value}</Typography>
              </Grid>
              
            </Grid>
  
            <Grid item>
              <Typography variant="subheading">{ formatMoney(total,2,'.',',') }</Typography>
            </Grid>
  
          </Grid>
  
          <Grid item>
            <ButtonBase className={classes.containerRemove} onClick={this.removeIndex}>
              <Typography className={classes.remove} gutterBottom variant="subheading">
                  ( - )
                </Typography>
            </ButtonBase>
          </Grid>
  
        </Grid>
      </Paper>
    );
  }
  
}

List.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  ...state
 })

const mapDispatchToProps = dispatch => ({
  removeData: (index) => dispatch(removeData(index)),
})

export default compose (
  withStyles(styles),
  connect(mapStateToProps,mapDispatchToProps)
)(List);