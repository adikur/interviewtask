import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { addData } from '../model/DataAction';
import { getDataCurrency } from '../model/CurrencyAction';

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: 300,
    padding: theme.spacing.unit * 2,
    marginTop: 5,
    marginBottom:5,
    borderWidth:1,

  },
  rootPicker: {
    flexGrow: 1,
    width: 300,
    paddingLeft:16,
    paddingRight:16,
    marginTop: 5,
    marginBottom:5,
    borderWidth:1,
  },
  continerSubmit: {
    marginTop:3,
    width:80,
    height: 50,
  },
  submit: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },
});

const options = [
    "Choose",
    "USD",
    "CAD",
    "IDR",
    "GBP",
    "CHF",
    "SGD",
    "INR",
    "MYR",
    "JPY",
    "KRW"
  ];
  
  const optionsName = [
    "",
    "United States Dollar",
    "Canadian Dollar",
    "Indonesian Rupiah",
    "British Pound",
    "Swiss Franch",
    "Singapore Dollar",
    "Indian Rupee",
    "Malaysian Ringgit",
    "Japanese Yen",
    "South Korean Won"
  ];

  class Add extends React.Component  {
    button = null;

    state = {
        isViewList : false,
        anchorEl: null,
        selectedIndex: 0,
        newData: null
    };
  
    handleClickListItem = event => {
      this.setState({ anchorEl: event.currentTarget });
    };
  
    handleMenuItemClick = (event, index) => {
        
        
        var rates = this.props.dataCurrency.rates
        var rate = rates[options[index]];
        console.log("result "+rate);


        this.setState({ 
                selectedIndex: index, 
                anchorEl: null ,

                newData: {
                    currency: options[index],
                    name: optionsName[index], 
                    value: rate,
                }
            });
    };


    submitData = () => {
        console.log("before "+JSON.stringify(this.state.newData));
        if(Boolean(this.state.newData)){
            this.props.addData(this.state.newData);
        
            // reset //
            this.setState({
                selectedIndex: 0,
                newData: null
            })
        }else{
            alert("Please choose currency...");
        }
        
        console.log("after "+JSON.stringify(this.state.newData));
    }
   
  
    handleClose = () => {
      this.setState({ anchorEl: null });
    };

    
    handleViewList = (event) => {
        this.setState({
            isViewList : !this.isViewList
        })
    }

    render() {
        const { classes } = this.props;
        const { anchorEl } = this.state;

    return (

            <div>
                {
                    (this.state.isViewList === false ) ? 
                    (
                        <Paper className={classes.root}>
                           <Grid container spacing={16}>
                             <Grid item xs={12} sm container>

                               <Grid item xs container direction="column" spacing={8}>
                                 <ButtonBase onClick={this.handleViewList}>
                                     <Grid item xs>
                                        
                                             <Typography gutterBottom variant="subheading" >
                                                 ( + )  Add More Currencies
                                             </Typography>
                                     </Grid>
                                 </ButtonBase>
                                
                               </Grid>


                             </Grid>

                           </Grid>
                        </Paper>
                    ):(

                        <Paper className={classes.rootPicker}>


                            <pre>
                            {
                                //JSON.stringify(this.props.dataCurrency) 
                            }
                            </pre>

                            <Grid container spacing={16}>
                                <Grid item xs={12} sm container>

                                <Grid item xs container direction="column" spacing={8}>

                                        <List component="nav">
                                        <ListItem
                                            button
                                            aria-haspopup="true"
                                            aria-controls="lock-menu"    
                                            onClick={this.handleClickListItem}
                                        >
                                            <ListItemText
                                                primary={options[this.state.selectedIndex]}                
                                            />
                                        </ListItem>
                                        </List>

                                        <Menu
                                            id="lock-menu"
                                            anchorEl={anchorEl}
                                            open={Boolean(anchorEl)}
                                            onClose={this.handleClose}
                                            >
                                            {options.map((option, index) => (
                                                <MenuItem
                                                key={option}
                                                disabled={index === 0}
                                                selected={index === this.state.selectedIndex}
                                                onClick={event => this.handleMenuItemClick(event, index)}
                                                >
                                                {option}
                                                </MenuItem>
                                            ))}
                                        </Menu>




                                    
                                </Grid>

                                <Grid item>
                                    <ButtonBase className={classes.continerSubmit} onClick={this.submitData}>
                                        <Typography className={classes.submit} gutterBottom variant="subheading">
                                            Submit
                                        </Typography>
                                    </ButtonBase>
                                    </Grid>


                                </Grid>

                            </Grid>
                        </Paper>
                    )
                }
                


                

            </div>

        );
    }
}

Add.propTypes = {
  classes: PropTypes.object.isRequired,
};



const mapStateToProps = state => ({
    ...state
   })

const mapDispatchToProps = dispatch => ({
    addData: (newData) => dispatch(addData(newData)),
    getDataCurrency: () => dispatch(getDataCurrency())
})

export default compose (
    withStyles(styles),
    connect(mapStateToProps,mapDispatchToProps)
)(Add);